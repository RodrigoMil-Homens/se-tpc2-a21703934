import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { MovieDetailPage } from '../movie-detail/movie-detail';
import { MoviesProvider } from '../../providers/movies-provider';
import { Movie } from '../../model/movie';
import { SearchPage } from '../search/search';

@Component({
  selector: 'page-wishlist',
  templateUrl: 'wishlist.html'
})
export class WishlistPage {
  resultado=0;
  movies: Movie[];

  constructor(public navCtrl: NavController, public events: Events, public moviesProvider: MoviesProvider) {
    // Filter movie data instead of showing entire set
    this.filterMovieData();
    // Subscribe data changes to reapply filter
    this.events.subscribe('movie-data-changed', (movie) => {
      this.filterMovieData();
    });
  }

  filterMovieData() {
    this.movies = this.moviesProvider.movies.filter((movie) => {
      // return true if wishlist flag is true, show and not filter
      return movie.wishlist;
    });
  }

  openMovieDetail(movie: any) {
    this.navCtrl.push(MovieDetailPage, { selectedMovie: movie });
	
	

  }
 
	  
  
  pagar(click){
	    alert("compra confirmada");
  }
apagar(click){
	  alert("Para apagar o artigo terá de ir aos detalhes do local e cancelar lá");
		this.navCtrl.push(SearchPage).then(() => {
		let index = 1;
		this.navCtrl.remove(index);
		});
	  
	  
  }
  
}