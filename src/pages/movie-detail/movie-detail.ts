import { Component } from '@angular/core';
import { NavParams, Events } from 'ionic-angular';
import { MoviesProvider } from '../../providers/movies-provider';
import { Movie } from '../../model/movie';

@Component({
  selector: 'page-movie-detail',
  templateUrl: 'movie-detail.html'
})
export class MovieDetailPage {
	quantidade = 1;
	preço = 0;
  movie: Movie;

  constructor(public navParams: NavParams, public events: Events, public moviesProvider: MoviesProvider) {
    this.movie = this.navParams.get('selectedMovie');
    console.log("Showing detail for movie '" + this.movie.title + "'");
  }
adicionar(click)	 {
		this.movie.wishlist= true;
		this.movie.watched=this.quantidade;
		this.preço=this.movie.runtime;
		this.movie.runtime = this.preço*this.quantidade;
		
		
    };
retirar(click)	 {
		this.movie.wishlist= false;
		this.movie.watched=this.quantidade;
		this.movie.runtime = this.preço;
		
    };
	

	
}
