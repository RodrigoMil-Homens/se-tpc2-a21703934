import { Component } from '@angular/core';

import { SearchPage } from '../search/search';

import { WishlistPage } from '../wishlist/wishlist';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = SearchPage;
  tab2Root = WishlistPage;


  constructor() {

  }
}
