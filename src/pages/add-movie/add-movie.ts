import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, NavParams, ViewController, ToastController} from 'ionic-angular';
import { MoviesProvider } from '../../providers/movies-provider';
import { Movie } from '../../model/movie';

@Component({
  selector: 'page-add-movie',
  templateUrl: 'add-movie.html',
})
export class AddMoviePage {

  movieForm: any;
  submitAttempt: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    public formBuilder: FormBuilder,
    public moviesProvider: MoviesProvider
  ) {

    // Tutorial with more complex FormBuilder and Validators at https://www.joshmorony.com/advanced-forms-validation-in-ionic-2/
    this.movieForm = formBuilder.group({
        title: ['', Validators.required],
        thumb: ['', Validators.required],
        genre: [''],
        watched: [''],
        runtime: ['']  
		
    });
  }

  saveMovie() {
    // Flag used to avoid displaying errors on first form appearance
    this.submitAttempt = true;

    // Validate form
    if(!this.movieForm.valid){
      console.log("Error on Form!")
      this.showToastNotification("Error on Form!");
    }
    else {
      // Create movie object
      let newMovie = new Movie(
        this.movieForm.value.title,
        this.movieForm.value.thumb,
        this.movieForm.value.genre,
        this.movieForm.value.watched,
        this.movieForm.value.runtime,
		this.movieForm.value.year,
		this.movieForm.value.sinopse,
		this.movieForm.value.director,
		this.movieForm.value.stars,
		this.movieForm.value.wishlist
      );
      // Save movie in provider and publish change
	  
	  
	  
	  
      this.moviesProvider.save(newMovie);
      this.moviesProvider.publishChange(newMovie);

	newMovie.genre= this.movieForm.value.genre;

	newMovie.runtime= this.movieForm.value.runtime;
	
      
	  // Show notification and log
      console.log("Movie '" + newMovie.title + "' Saved!")
      this.showToastNotification("Movie '" + newMovie.title + "' Saved!");
      // Go back to previous screen
      this.viewCtrl.dismiss();
    
	 
	
	
	}
  }


  showToastNotification(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

}
