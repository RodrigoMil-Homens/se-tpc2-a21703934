import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MovieDetailPage } from '../movie-detail/movie-detail';
import { AddMoviePage } from '../add-movie/add-movie';
import { WishlistPage } from '../wishlist/wishlist';
import { MoviesProvider } from '../../providers/movies-provider';
import { Movie } from '../../model/movie';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

  movies: Movie[];

  constructor(
    public navCtrl: NavController,
    public moviesProvider: MoviesProvider
  ) {
    this.movies = this.moviesProvider.movies;
  }

  openMovieDetail(movie: any) {
    this.navCtrl.push(MovieDetailPage, { selectedMovie: movie });
  }
  
  openMovieAdd() {
    this.navCtrl.push(AddMoviePage, {});
  }


  onSearchInputChanged(event: any) {
    // get the value of the searchbar and log it
    let searchQuery = event.target.value;
    console.log("onSearchInputChanged '" + searchQuery + "'");

    //TODO: Finish implementation of search functionality
  }

  addWish(movie: Movie) {
    //TODO: Implement change of wishlist flag
  }

  addWatched(movie: Movie) {
    //TODO: Implement change of watched flag
  }
}
