
export class Movie {
/*
  Sample JSON structure
  {
    "title": "Sharknado",
    "thumb": "https://images-na.ssl-images-amazon.com/images/M/MV5BOTE2OTk4MTQzNV5BMl5BanBnXkFtZTcwODUxOTM3OQ@@._V1_SY1000_CR0,0,712,1000_AL_.jpg",
    "year": 2013,
    "sinopse": "When a freak hurricane swamps Los Angeles, nature's deadliest killer rules sea, land, and air as thousands of sharks terrorize the waterlogged populace.",
    "director": "Anthony C. Ferrante",
    "runtime": 86,
    "stars": "Tara Reid",
    "genre": "Action, Adventure, Comedy",
    "watched": false,
    "wishlist": false
  }
*/
  constructor(
    public title: string,
    public thumb: string,
    public year: number,
    public sinopse: string,
    public director: string,
    public runtime: number,
    public stars: string,
    public genre: string,
    public watched: number,
    public wishlist: boolean) {
  }

}
