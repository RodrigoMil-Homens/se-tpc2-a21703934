import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SearchPage } from '../pages/search/search';
import { TabsPage } from '../pages/tabs/tabs';
import { MoviesProvider } from '../providers/movies-provider';

@Component({
  templateUrl: 'app.html',
   // Provider Injected globally. Will share state across different Components (pages)
  providers: [MoviesProvider]
})
export class MyApp {
  rootPage:any = TabsPage;

//  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, moviesProvider: MoviesProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
